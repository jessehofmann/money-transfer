﻿using System;
using System.Collections.Generic;
using TenmoClient.APIClients;
using TenmoClient.Data;

namespace TenmoClient
{
    public class UserInterface
    {
        private readonly ConsoleService consoleService = new ConsoleService();
        private readonly AuthService authService = new AuthService();
        private readonly AccountsAPI accountsAPI = new AccountsAPI();
        private readonly UsersAPI usersAPI = new UsersAPI();
        private readonly TransferAPI transferAPI = new TransferAPI();

        private bool shouldExit = false;

        public void Start()
        {
            while (!shouldExit)
            {
                while (!authService.IsLoggedIn)
                {
                    ShowLogInMenu();
                }

                // If we got here, then the user is logged in. Go ahead and show the main menu
                ShowMainMenu();
            }
        }

        private void ShowLogInMenu()
        {
            Console.WriteLine("Welcome to TEnmo!");
            Console.WriteLine("1: Login");
            Console.WriteLine("2: Register");
            Console.Write("Please choose an option: ");

            if (!int.TryParse(Console.ReadLine(), out int loginRegister))
            {
                Console.WriteLine("Invalid input. Please enter only a number.");
            }
            else if (loginRegister == 1)
            {
                HandleUserLogin();
            }
            else if (loginRegister == 2)
            {
                HandleUserRegister();
            }
            else
            {
                Console.WriteLine("Invalid selection.");
            }
        }

        private void ShowMainMenu()
        {
            int menuSelection = -1;
            while (menuSelection != 0)
            {
                Console.WriteLine();
                Console.WriteLine("Welcome to TEnmo! Please make a selection: ");
                Console.WriteLine("1: View your current balance");
                Console.WriteLine("2: View your past transfers");
                Console.WriteLine("3: View your pending requests");
                Console.WriteLine("4: Send TE bucks");
                Console.WriteLine("5: Request TE bucks");
                Console.WriteLine("6: Log in as different user");
                Console.WriteLine("0: Exit");
                Console.WriteLine("---------");
                Console.Write("Please choose an option: ");

                if (!int.TryParse(Console.ReadLine(), out menuSelection))
                {
                    Console.WriteLine("Invalid input. Please enter only a number.");
                }
                else
                {
                    switch (menuSelection)
                    {
                        case 1:
                            DisplayBalance();
                            break;
                        case 2:
                            GetTransfers(); ; // TODO: Implement me
                            break;
                        case 3:
                            Console.WriteLine("NOT IMPLEMENTED!"); // TODO: Implement me
                            break;
                        case 4:
                            SendBucks();
                            break;
                        case 5:
                            Console.WriteLine("NOT IMPLEMENTED!"); // TODO: Implement me
                            break;
                        case 6:
                            Console.WriteLine();
                            UserService.SetLogin(new API_User()); //wipe out previous login info
                            return;
                        default:
                            Console.WriteLine("Goodbye!");
                            shouldExit = true;
                            return;
                    }
                }
            }
        }

        private void HandleUserRegister()
        {
            bool isRegistered = false;

            while (!isRegistered) //will keep looping until user is registered
            {
                LoginUser registerUser = consoleService.PromptForLogin();
                isRegistered = authService.Register(registerUser);
            }

            Console.WriteLine("");
            Console.WriteLine("Registration successful. You can now log in.");
        }

        private void HandleUserLogin()
        {
            while (!UserService.IsLoggedIn) //will keep looping until user is logged in
            {
                LoginUser loginUser = consoleService.PromptForLogin();
                API_User user = authService.Login(loginUser);
                if (user != null)
                {
                    UserService.SetLogin(user);
                }
            }
        }

        private void DisplayBalance()
        {
            Account account = new Account();

            try
            {
                account = accountsAPI.GetAccount();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Your current account balance is: $" + account.Balance);
            Console.WriteLine();

        }

        private void SendBucks()
        {
            List<User> users = usersAPI.GetUsers();
            Account account = accountsAPI.GetAccount();
            bool isNumber = false;
            bool isAccountNumber = false;
            decimal desiredAmount = 0;
            int desiredUserId = 0;
            int count = 0;

            Console.WriteLine("---------------------------");
            Console.WriteLine();
            Console.Write("Users ID".PadRight(15));
            Console.Write("Name");
            Console.WriteLine();
            Console.WriteLine("---------------------------");
            foreach (User user in users)
            {
                if (user.UserId == account.UserId)
                {

                }
                else
                {
                    Console.WriteLine();
                    Console.Write($"{user.UserId}".PadRight(15));
                    Console.Write($"{user.Username}");
                }
            }
            Console.WriteLine();
            Console.Write("Enter ID of the user you are sending to (0 to cancel): ");
            while (!isAccountNumber)
            {
                string userAccountInput = Console.ReadLine();
                isAccountNumber = int.TryParse(userAccountInput, out desiredUserId);
                if (!isAccountNumber)
                {
                    Console.WriteLine("Please enter valid number");
                }
            }
            if (desiredUserId == 0)
            {
                return;
            }
            foreach(User user in users)
            {
                if(user.UserId == desiredUserId && desiredUserId != account.UserId)
                {
                    count++;
                }
            }
            if(count == 0)
            {
                Console.WriteLine("Invalid Account");
                return;
            }
            Account destinationAccount = accountsAPI.GetDestinationAccount(desiredUserId);
            Console.Write("Enter amount: ");


            while (!isNumber)
            {
                string userInput = Console.ReadLine();
                isNumber = decimal.TryParse(userInput, out desiredAmount);
                if (!isNumber)
                {
                    Console.WriteLine("Please enter valid number");
                }
            }        
            if (desiredAmount > account.Balance)
            {
                Console.WriteLine($"DENIED: Insufficient funds. You cannot send more than your current balance of ${account.Balance}.");
            }
            else if (desiredAmount <= 0)
            {
                Console.WriteLine("DENIED: You cannot send negative or 0 money");
            }
            else
            {
                accountsAPI.AccountMinus(desiredAmount, account);
                accountsAPI.AccountPlus(desiredAmount, destinationAccount);
                Transfer temp = new Transfer();
                temp.AccountFromId = account.AccountId;
                temp.AccountToId = destinationAccount.AccountId;
                temp.Amount = desiredAmount;
                temp.TransferStatus = "Approved";
                temp.TransferType = "Send";
                transferAPI.AddTransfer(temp);
                Console.WriteLine("TE bucks successfully sent!");
            }
        }

        private void GetTransfers()
        {
            Account account = accountsAPI.GetAccount();
            List<Transfer> transfers = transferAPI.GetTransfers();
            bool isNumber = false;
            int transferId = -1;

            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine();
            Console.Write("Transfer ID".PadRight(15));
            Console.Write("From/To".PadRight(30));
            Console.Write("Amount");
            Console.WriteLine();
            Console.WriteLine("---------------------------------------------------");
            Console.WriteLine();

            foreach (Transfer transfer in transfers)
            {
                if (transfer.AccountFromId == account.AccountId)
                {
                    User toUser = usersAPI.GetUserFromAccountId(transfer.AccountToId);
                    Console.Write($"{transfer.Id}".PadRight(15));
                    Console.Write($"To: {toUser.Username}".PadRight(30));
                    Console.Write($"{transfer.Amount:c}");
                    Console.WriteLine();
                }

                if (transfer.AccountToId == account.AccountId)
                {
                    User fromUser = usersAPI.GetUserFromAccountId(transfer.AccountFromId);
                    Console.Write($"{transfer.Id}".PadRight(15));
                    Console.Write($"From: {fromUser.Username}".PadRight(30));
                    Console.Write($"{transfer.Amount:c}");
                    Console.WriteLine();
                }
            }
            Console.WriteLine();
            Console.Write("Please enter transfer ID to view details (0 to cancel): ");
            while (!isNumber)
            {
                string userInput = Console.ReadLine();
                isNumber = int.TryParse(userInput, out transferId);
                if (!isNumber)
                {
                    Console.WriteLine("Please enter valid number");
                }
            }
            if (transferId == 0)
            {
                return;
            }

            GetTransferDetails(transferId);
        }

        private void GetTransferDetails(int transferId)
        {
            List<Transfer> transfers = transferAPI.GetTransfers();
            int count = 0;


            foreach (Transfer transfer in transfers)
            {
                if (transfer.Id == transferId)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Id: {transfer.Id}");
                    User fromUser = usersAPI.GetUserFromAccountId(transfer.AccountFromId);
                    Console.WriteLine($"From {fromUser.Username}");
                    User toUser = usersAPI.GetUserFromAccountId(transfer.AccountToId);
                    Console.WriteLine($"To: {toUser.Username}");
                    Console.WriteLine($"Type: {transfer.TransferType}");
                    Console.WriteLine($"Status: {transfer.TransferStatus}");
                    Console.WriteLine($"Amount: ${transfer.Amount}");
                    count++;
                }
            }
            if (count < 1)
            {
                Console.WriteLine("Please select a valid Transfer ID");
            }
        }
    }
}
