﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using TenmoClient.Data;

namespace TenmoClient.APIClients
{
    class TransferAPI : AuthService
    {
        private readonly string API_URL = API_BASE_URL + "transfer";

        public List<Transfer> GetTransfers()
        {
            RestRequest request = new RestRequest(API_URL + "/" + userid);
            IRestResponse<List<Transfer>> response = client.Get<List<Transfer>>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new Exception("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }
        public bool AddTransfer(Transfer transfer)
        {
            RestRequest request = new RestRequest(API_URL);
            request.AddJsonBody(transfer);

            IRestResponse response = client.Post(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                return false;
            }
            else if (!response.IsSuccessful)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
