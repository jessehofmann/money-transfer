﻿using System;
using System.Collections.Generic;
using System.Text;
using RestSharp;
using RestSharp.Authenticators;
using System;
using TenmoClient.Data;

namespace TenmoClient.APIClients
{
    class AccountsAPI : AuthService
    {
        private readonly string API_URL = API_BASE_URL + "account";
        public Account GetAccount()
        {
            //Account account = new Account(); 


            RestRequest request = new RestRequest(API_URL + "/" + userid);
            IRestResponse<Account> response = client.Get<Account>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new Exception("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }
        public Account GetDestinationAccount(int desiredUserId)
        {
            Account destinationAccount = new Account();

            RestRequest request = new RestRequest(API_URL + "/" + desiredUserId);
            IRestResponse<Account> response = client.Get<Account>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new Exception("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }
        public bool AccountMinus(decimal amount, Account account)
        {
            account.Balance -= amount;

            RestRequest request = new RestRequest(API_URL);
            request.AddJsonBody(account);

            IRestResponse response = client.Put(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                return false;
            }
            else if (!response.IsSuccessful)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool AccountPlus(decimal amount, Account destinationAccount)
        {
            destinationAccount.Balance += amount;

            RestRequest request = new RestRequest(API_URL + "/add");
            request.AddJsonBody(destinationAccount);

            IRestResponse response = client.Put(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                return false;
            }
            else if (!response.IsSuccessful)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
