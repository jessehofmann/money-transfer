﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using TenmoClient.Data;

namespace TenmoClient.APIClients
{
    class UsersAPI : AuthService
    {
        private readonly string API_URL = API_BASE_URL + "Users";
        
        public List<User> GetUsers()
        {
            List<User> users = new List<User>();

            RestRequest request = new RestRequest(API_URL);
            IRestResponse<List<User>> response = client.Get<List<User>>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new Exception("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }

        public User GetUserFromAccountId(int accountId)
        {
            User user = new User();

            RestRequest request = new RestRequest(API_URL + "/" + accountId);
            IRestResponse<User> response = client.Get<User>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new Exception("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }





        public Account GetAccount()
        {
            //Account account = new Account(); 


            RestRequest request = new RestRequest(API_URL + "/" + userid);
            IRestResponse<Account> response = client.Get<Account>(request);
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                throw new Exception("Error occurred - unable to reach server.");
            }
            else if (!response.IsSuccessful)
            {
                throw new Exception("Error occurred - received non-success response: " + (int)response.StatusCode);
            }
            else
            {
                return response.Data;
            }
        }
    }
}
