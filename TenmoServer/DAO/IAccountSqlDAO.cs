﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public interface IAccountSqlDAO
    {
        Account GetAccount(int id);
        bool AccountMinus(Account account);
        bool AccountPlus(Account destinationAccount);
    }
}
