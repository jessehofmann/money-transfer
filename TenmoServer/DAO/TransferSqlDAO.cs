﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public class TransferSqlDAO : ITransferSqlDAO
    {
        private readonly string connectionString;
        const string sql_GetTransfers = "SELECT * FROM transfers t " +
                    "JOIN transfer_statuses ts ON t.transfer_status_id = ts.transfer_status_id " +
                    "JOIN transfer_types tt ON t.transfer_type_id = tt.transfer_type_id " +
                    "JOIN accounts a ON a.account_id = t.account_from OR a.account_id = t.account_to " +
                    "JOIN users u ON u.user_id = a.user_id " +
                    "WHERE a.user_id = @user";

        const string sql_AddTransfer = "INSERT INTO transfers(transfer_type_id, transfer_status_id, account_from, account_to, amount) " +
                                    "VALUES(1001, 2001, @account_from, @account_to, @amount)";



        public TransferSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public List<Transfer> GetTransfers(int userid)
        {
            List<Transfer> transfers = new List<Transfer>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql_GetTransfers, conn);
                    cmd.Parameters.AddWithValue("@user", userid);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Transfer transfer = ReaderToTransfer(reader);
                        transfers.Add(transfer);
                    }
                }
            }
            catch (Exception ex)
            {
                transfers = new List<Transfer>();
            }

            return transfers;
        }
        private Transfer ReaderToTransfer(SqlDataReader reader)
        {
            Transfer transfer = new Transfer();
            transfer.Id = Convert.ToInt32(reader["transfer_id"]);
            transfer.AccountFromId = Convert.ToInt32(reader["account_from"]);
            transfer.AccountToId = Convert.ToInt32(reader["account_to"]);
            transfer.Amount = Convert.ToDecimal(reader["amount"]);
            transfer.TransferStatus = Convert.ToString(reader["transfer_status_desc"]);
            transfer.TransferType = Convert.ToString(reader["transfer_type_desc"]);
            return transfer;
        }

        public bool AddTransfer(Transfer transfer)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sql_AddTransfer, conn);
                    cmd.Parameters.AddWithValue("@account_from", transfer.AccountFromId);
                    cmd.Parameters.AddWithValue("@account_to", transfer.AccountToId);
                    cmd.Parameters.AddWithValue("@amount", transfer.Amount);

                    int count = cmd.ExecuteNonQuery();

                    if (count > 0)
                    {
                        result = true;
                    }
                }
            }
            catch(Exception ex)
            {
                result = false;
            }

            return result;
        }
    }
}
