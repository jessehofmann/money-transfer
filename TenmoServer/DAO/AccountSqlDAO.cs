﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TenmoServer.Models;

namespace TenmoServer.DAO
{
    public class AccountSqlDAO : IAccountSqlDAO
    {
        private readonly string connectionString;

        const string sql_getAccount = "SELECT * FROM accounts WHERE user_id = @user_id";
        const string sql_updateAccount = "UPDATE accounts " +
                                            "SET balance = @newBalance " +
                                            "WHERE user_id = @user";

        public AccountSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }


        public Account GetAccount(int id)
        {
            Account userAccount = null;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    
                    SqlCommand cmd = new SqlCommand(sql_getAccount, conn);
                    cmd.Parameters.AddWithValue("@user_id", id);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows && reader.Read())
                    {
                        userAccount = GetAccountFromReader(reader);
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return userAccount;
        }

        private Account GetAccountFromReader(SqlDataReader reader)
        {
            Account a = new Account();

            a.UserId = Convert.ToInt32(reader["user_id"]);
            a.AccountId = Convert.ToInt32(reader["account_id"]);
            a.Balance = Convert.ToDecimal(reader["balance"]);

            return a;
        }

        public bool AccountMinus(Account account)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_updateAccount, conn);
                    cmd.Parameters.AddWithValue("@newBalance", account.Balance);
                    cmd.Parameters.AddWithValue("@user", account.UserId);

                    int works = cmd.ExecuteNonQuery();
                    if(works > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return result;
        }
        public bool AccountPlus(Account destinationAccount)
        {
            bool result = false;
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(sql_updateAccount, conn);
                    cmd.Parameters.AddWithValue("@newBalance", destinationAccount.Balance);
                    cmd.Parameters.AddWithValue("@user", destinationAccount.UserId);

                    int works = cmd.ExecuteNonQuery();
                    if (works > 0)
                    {
                        result = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                throw;
            }
            return result;
        }
    }
}




