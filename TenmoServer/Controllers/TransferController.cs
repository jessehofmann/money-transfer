﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TenmoServer.DAO;
using TenmoServer.Models;

namespace TenmoServer.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class TransferController : ControllerBase
    {
        private ITransferSqlDAO transferSqlDAO;

        public TransferController(ITransferSqlDAO transferSqlDAO)
        {
            this.transferSqlDAO = transferSqlDAO;
        }
        [HttpGet("{userid}")]
        public ActionResult<List<Transfer>> GetTransfers(int userid)
        {
            return Ok(transferSqlDAO.GetTransfers(userid));
        }
        [HttpPost]
        public ActionResult AddTransfer(Transfer transfer)
        {

            bool result = transferSqlDAO.AddTransfer(transfer);

            if (result)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
