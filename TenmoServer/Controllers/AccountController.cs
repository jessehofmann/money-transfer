﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TenmoServer.DAO;
using TenmoServer.Models;
using TenmoServer.Security;
using Microsoft.AspNetCore.Authorization;

namespace TenmoServer.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private IAccountSqlDAO accountDAO;

        public AccountController(IAccountSqlDAO accountDAO)
        {
            this.accountDAO = accountDAO;
        }

        [HttpGet("{userid}")]
        public ActionResult<Account> GetAccount(int userid)
        {
            return Ok(accountDAO.GetAccount(userid));
        }

        [HttpPut]
        public ActionResult AccountMinus(Account account)
        {
            bool result = accountDAO.AccountMinus(account);
            if (result)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpPut("add")]
        public ActionResult AccountPlus(Account destinationAccount)
        {
            bool result = accountDAO.AccountPlus(destinationAccount);
            if (result)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
