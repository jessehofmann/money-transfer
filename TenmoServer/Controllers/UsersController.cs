﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TenmoServer.DAO;
using TenmoServer.Models;

namespace TenmoServer.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserDAO userDAO;

        public UsersController(IUserDAO userDAO)
        {
            this.userDAO = userDAO;
        }

        [HttpGet]
        public ActionResult<List<User>> GetUsers()
        {
            return Ok(userDAO.GetUsers());
        }

        [HttpGet("{accountId}")]
        public ActionResult<User> GetUserFromAccountId(int accountId)
        {
            return Ok(userDAO.GetUserFromAccountId(accountId));
        }
    }
}
